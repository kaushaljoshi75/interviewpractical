<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('user_register', 'RegisterController@index');
Route::Post('user_register_store', 'RegisterController@store');
Route::get('order/index', 'HomeController@index')->name('order/index');
Route::get('order/create', 'HomeController@create')->name('order/create');
Route::Post('order/store', 'HomeController@store')->name('order/store');
Route::get('order/edit/{id}', 'HomeController@edit');
Route::Post('order/update', 'HomeController@update')->name('order/update');
Route::get('order/delete/{id}', 'HomeController@delete');
Route::get('/serach', 'HomeController@search');

