<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create roles
        $roles = [
        	[
        		'name' => 'admin',
        		'display_name' => 'Admin',
        		'description' => 'Admin'
        	],
            [
        		'name' => 'customer',
        		'display_name' => 'Customer',
        		'description' => 'Customer'
        	]
        ];

        foreach ($roles as $key => $value) {
        	Role::create($value);
        }
    }
}
