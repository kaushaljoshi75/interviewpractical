<?php

use Illuminate\Database\Seeder;
use App\Cars;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = [
        	[
        		'company_name' => 'TATA Motors',
        		'cars_name' => 'Indica',
        		'cars_model' => 'Indica Vista Spots'
        	],
        	[
        		'company_name' => 'Maruti Suzuki',
        		'cars_name' => 'Maruti Suzuki Baleno',
        		'cars_model' => 'Delta Petrol'
        	],
        	[
        		'company_name' => 'Mahindra & Mahindra',
        		'cars_name' => 'XUV 500',
        		'cars_model' => 'Spots'
        	],
        	[
        		'company_name' => 'Hyundai',
        		'cars_name' => 'I 20',
        		'cars_model' => 'Magma'
			],
			[
        		'company_name' => 'Volkswagen',
        		'cars_name' => 'Polo',
        		'cars_model' => 'Polo sports'
			],
			[
        		'company_name' => 'Honda',
        		'cars_name' => 'Amaze',
        		'cars_model' => 'Sigma Petrol'
			],
			[
        		'company_name' => 'Renault India',
        		'cars_name' => 'Kwid',
        		'cars_model' => 'Spots'
        	]
        ];

        foreach ($cars as $key => $value) {
        	Cars::create($value);
        }
    }
}
