@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <br />
        <div align="center">
            <h3 aling="center">Edit Customer</h3>
            <br />
            <form method="post" action="{{ URL('order/update') }}" id="m_form_1">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="text" value="{{$orders->display_name}}" name="display_name" class="form-control" placeholder="Enter Full Name" style="width:55%;">
                    @if ($errors->has('display_name'))
                        <strong>{{ $errors->first('display_name') }}</strong>
                    @endif
                </div>
                <div class="form-group">
                    <input type="email" value="{{$orders->email}}" name="email" class="form-control" placeholder="Enter Email" style="width:55%;"/>
                    @if ($errors->has('email'))
                        <strong>{{ $errors->first('email') }}</strong>
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" value="{{$orders->phone}}" name="phone" class="form-control" placeholder="Enter phone" style="width:55%;"/>
                    @if ($errors->has('phone'))
                        <strong>{{ $errors->first('phone') }}</strong>
                    @endif
                </div>
                <div class="form-group">
                    <select name="cars_id" id="cars_id" class="selectpicker" style="width:55%;">
                        @foreach ($carsName as $carsNameKey => $carsNameValue)
                            <option value="{{ $carsNameKey }}" {{ $orders->cars_id ==  $carsNameKey ? 'selected' : '' }}>{{ $carsNameValue }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit"  class="btn btn-primary" />
                    <input type="hidden" value= "{{$orders->user_id }}" name="userId" />
                <a href="{{ URL('order/index') }}" class="btn btn-primary" >Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection