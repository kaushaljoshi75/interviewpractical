@extends('layouts.app')


<?php /*Load css to header section*/ ?>


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    <div class="container box">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                    <div align="right">
                                            <a href="{{ URL('order/create') }}" class="btn btn-primary">{{ __('Add Customer') }}</a>
                                    </div>
                                <div class="form-group" style="margin-top:30px;">
                                <form method="get" action="{{ URL('/serach') }}" id="searchForm" role="form">
                                    <div class="input-group">
                                        <input type="text" name="searchValue" id="searchValue" class="form-control" placeholder="Search"  style="width:55%;" />
                                        <span class="input-group-prepand">
                                            <input type="submit" class="btn btn-primary btn_gry">
                                        </span>
                                    </div>
                                </form>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id="orders">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Car Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody class="search">
                                                @foreach($orders as $row)
                                                <tr>
                                                    <td>{{$row->display_name}}</td>
                                                    <td>{{$row->email}}</td>
                                                    <td>{{$row->phone}}</td>
                                                    <td>{{$row->carWeb->cars_name}}</td>
                                                    <td><a href="{{URL('order/edit/' . $row->user_id)}}" class="btn btn-warning" >Edit</a> | <a href="{{URL('order/delete/' . $row->order_id)}}" class="btn btn-danger">Delete</a></td>
                                                </tr>
                                                @endforeach
                                                <span class="pull-right">{{ $orders->links() }}</span>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('inc_script')

@endpush

