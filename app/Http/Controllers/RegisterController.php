<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\User;
use App\Role;
use Redirect;
use Session;
use Hash;
use Validator;
use Illuminate\Support\Facades\Crypt;
use DB;
use Auth;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Registrar bushiness owner
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'display_name' => 'required',
            'phone' => "required|unique:users,phone",
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|max:16',
        ];


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('user_register')
						->withErrors($validator)
                        ->withInput();
        } else {
            $admin = array(
                'display_name' => $request->get('display_name') ,
                'email' => $request->get('email') ,
                'password' => Hash::make($request->get('password')) ,
                'phone' => $request->get('phone') ,
            );
            $user = User::create($admin);
            $user->roles()->attach(Role::where('name', 'admin')->first());
            return Redirect::to('login')->with('message', 'Admin has been created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
