<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cars;
use App\User;
use App\Orders;
use App\Role;
use App\RoleUser;
use Redirect;
use Validator;
use Session;
use Hash;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Crypt;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders =  User::with('carWeb')->whereHas('roles', function ($q)
		{
			$q->where('name', 'customer');
		})->paginate('10');
        return view('home')->with('orders', $orders);
    }

    public function search(Request $request)
    {
        $search = $request->get('searchValue');
        $orders =  User::where('display_name','like' , '%'.$search.'%')
        ->orWhere('email','like' , '%'.$search.'%')
        ->orWhere('phone','like' , '%'.$search.'%')
        ->with('carWeb')
        ->whereHas('roles', function ($q)
		{
			$q->where('name', 'customer');
		})->paginate('10');
        return view('home', ['orders' => $orders]);
    }


    

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return void
	 */
	public function create()
	{
        $carsName = Cars::pluck('cars_name', 'cars_id');
		return view('create')->with('carsName', $carsName);
    }

    public function store(Request $request)
	{

        
        $validator = Validator::make($request->all(), [
            'display_name' => 'required|max:30',
            'email' => 'required|unique:users,email',
            'phone' => "required|unique:users,phone|max:13",
            'password' => 'required|min:6|max:16',
        ]);

        if ($validator->fails()) {
            return redirect('order/create')
						->withErrors($validator)
                        ->withInput();
        } else {
            $customer = array(
                'display_name' => $request->get('display_name') ,
                'email' => $request->get('email') ,
                'password' => Hash::make($request->get('password')) ,
                'phone' => $request->get('phone') ,
                'cars_id' => $request->get('cars_id'),
            );
            $user = User::create($customer);
            $user->roles()->attach(Role::where('name', 'customer')->first());

            return redirect()->route('order/index')->with('message', 'User created successfully');
        }
    }

    

    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return void
	 */
	public function edit($id)
	{
        $carsName = Cars::pluck('cars_name', 'cars_id');
        $orders = User::where('user_id', $id)->with('carWeb')->firstOrFail();
        
		return view('edit', compact('carsName'))->with('orders', $orders);;
    }
    

    public function update(Request $request)
	{

        $user = User::find($request->get('userId'));
        $validator = Validator::make($request->all(), [
            'display_name' => 'required|max:30',
            'email' => [
                'required',
			    Rule::unique('users')->ignore($request->get('userId'), 'user_id', 'email'),
            ],
            'phone' => [
                'required',
			    Rule::unique('users')->ignore($request->get('userId'), 'user_id', 'phone'),
            ],
        ]);

        if ($validator->fails()) {
            return redirect('order/edit/'. $request->get('userId'))
						->withErrors($validator)
                        ->withInput();
        } else {
            $user->display_name = $request->get('display_name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->cars_id = $request->get('cars_id');
            $user->save();

            return redirect()->route('order/index')->with('message', 'User updated successfully');
        }
    }

    public function delete($id)
	{
        $orders = User::where('user_id', $id)->delete();
        
		return redirect()->route('order/index')->with('message', 'User deleted successfully');
    }

}
