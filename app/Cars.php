<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{

    public $timestamps = false;

    protected $primaryKey = 'cars_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name','cars_name', 'cars_model'
    ];
    
    protected $hidden = [

    ];
}
