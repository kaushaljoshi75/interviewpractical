// Update orders
// if ($('.ready-id').val() == 0) {
//     $( ".ready-id" ).prop( "checked", false );
// } else {
//     $( ".ready-id" ).prop( "checked", true );
// }

$('.ready-id').change(function() {
    var checked_status = '0';
    var is_ready_val = $(this).val();
    var order_id = $(this).attr('data-id');

    if ($(this).is(":checked")) {
        checked_status = '1';
    } else {
        checked_status = '0';
    }

    $.ajax({
        url: site_url+"/owner/update/order",
        mehtod: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrfToken"]').attr('content')
        },
        data:{is_ready_val: is_ready_val,order_id:order_id,checked_status:checked_status} ,
        success: function(data) {
            // $('.confirmedOrder').modal('hide');
            swal({
                title: "",
                text: "Order has been updated successfully.",
                icon: "success",
                button: true,
                successMode: true,
                closeOnClickOutside: false,
            })
        },
    });
    return false;
});

// $(document).on('click', '.updateOrder', function() {
//     var is_ready_val = $('.ready-id').val();
//     var order_id = $('.food-order-id').val()
//     $.ajax({
//         url: site_url+"/owner/update/order",
//         mehtod: "GET",
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrfToken"]').attr('content')
//         },
//         data:{is_ready_val: is_ready_val,order_id:order_id} ,
//         success: function(data) {
//             // $('.confirmedOrder').modal('hide');
//             swal({
//                 title: "",
//                 text: "Order has been updated successfully.",
//                 icon: "success",
//                 button: true,
//                 successMode: true,
//                 closeOnClickOutside: false,
//             })
//         },
//     });
//     return false;
// });