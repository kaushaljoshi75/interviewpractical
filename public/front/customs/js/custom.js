// header fixed js
$(window).scroll(function () {
    var sticky = $('.header, .left_sidebar'),
        scroll = $(window).scrollTop();

    if (scroll >= 100) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
});

// login
function myFunction1() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

// reset password
function myFunction2() {
    var x = document.getElementById("new_password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function myFunction3() {
    var x = document.getElementById("cnfrm_password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

// add manager
function myFunction4() {
    var x = document.getElementById("add_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function myFunction5() {
    var x = document.getElementById("edit_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function myFunction6() {
    var x = document.getElementById("add_waiter");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function myFunction7() {
    var x = document.getElementById("edit_waiter");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
// upload gst certificate
$('#chooseFile').bind('change', function () {
    var filename = $("#chooseFile").val();
    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});

// upload images
$(document).ready(function () {
    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".file-upload-nw").on('change', function () {
        readURL(this);
    });

    $(".profile-pic").on('click', function () {
        $(".file-upload-nw").click();
    });
    $(".change-pic").on('click', function () {
        $(".file-upload-nw").click();
    });

});

// datepicker js
/*
$(function () {
    $('#datetimepicker1').datetimepicker({
        format: 'DD-MM-YYYY LT'
    });
    $('#datetimepicker2').datetimepicker({
        format: 'DD-MM-YYYY'
    });
});
*/

$(document).ready(function () {
    // $('#example').DataTable( {
    //     columnDefs: [ {
    //         orderable: false,
    //         className: 'select-checkbox',
    //         targets:   0
    //     } ],
    //     select: {
    //         style:    'os',
    //         selector: 'td:first-child'
    //     },
    //     order: [[ 1, 'asc' ]]
    // } );
});

////EXPORT popup js
var $menu = $('.slct_drop_box');

$('.cstm-drpdwn').click(function () {
    $menu.toggle();
});

$(document).mouseup(function (e) {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
        && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $menu.hide();
    }
});
/////counter-js
Number.prototype.comma = function() {
    var parts = this.toString().split(".");
    console.log(parts);
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
};

$('[data-counter]').each(function () {
    var num = $(this).data('counter');

    $(this).prop('Counter',num*0.8).animate({
        Counter: num
    }, {
        duration: 2000,
        easing: 'swing',
        step: function (now) {
            var char = num.toString();
            var char1 = num[char.length-1];
            if (char1 != undefined) {
                $(this).text(Math.ceil(now).toLocaleString()+char1);
            } else {
                // $(this).text(Math.ceil(now).comma());
                $(this).text(Math.ceil(now).toLocaleString());
            }
        }
    });
});
